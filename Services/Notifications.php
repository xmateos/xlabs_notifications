<?php

namespace XLabs\NotificationsBundle\Services;

use XLabs\NotificationsBundle\Client\Client as Elephant;

class Notifications
{
    /*
     * Service to generate NodeJS notifications
     */
    private $config;
    private $socket;

    public function __construct($config, Elephant $socket)
    {
        $this->config = $config;
        $this->socket = $socket;
    }

    public function closeSocket()
    {
        $this->socket->getElephantIO()->close();
    }

    public function isAlive()
    {
        $url = 'https://'.$this->config['nodejs_settings']['host'].'/check_availability';

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch,  CURLOPT_RETURNTRANSFER, true);

        $response = curl_exec($ch);
        $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);

        curl_close($ch);
        if($httpCode >= 200 && $httpCode <= 400) {
            return true;
        } else {
            return false;
        }
    }

    public function showNotification($aOptions)
    {
        if(!$this->config['enabled'] || !$this->isAlive())
        {
            return false;
        }

        $default_options = array(
            'destinatary_id' => false,
            'title' => false,
            'msg' => 'Test text for message',
            'icon' => '/bundles/xlabsnotify/xlabs.png',
            'mute' => false,
            'link' => false,
            'sound' => false,
            'fadeout_after_ms' => false
        );
        $aOptions = array_merge($default_options, $aOptions);
        //$aOptions['destinatary_id'] = $aOptions['destinatary_id'] ? md5($aOptions['destinatary_id']) : $aOptions['destinatary_id'];

        $this->socket->send('notification', $aOptions);
        $this->closeSocket();
        return true;
    }

    public function playSound($aOptions)
    {
        if(!$this->config['enabled'] || !$this->isAlive())
        {
            return false;
        }

        $default_options = array(
            'destinatary_id' => false,
            'file' => false,
        );
        $aOptions = array_merge($default_options, $aOptions);
        //$aOptions['destinatary_id'] = $aOptions['destinatary_id'] ? md5($aOptions['destinatary_id']) : $aOptions['destinatary_id'];

        $this->socket->send('play_sound', $aOptions);
        $this->closeSocket();
        return true;
    }

    public function speakText($aOptions)
    {
        if(!$this->config['enabled'] || !$this->isAlive())
        {
            return false;
        }

        $default_options = array(
            'destinatary_id' => false,
            'text' => false,
        );
        $aOptions = array_merge($default_options, $aOptions);
        //$aOptions['destinatary_id'] = $aOptions['destinatary_id'] ? md5($aOptions['destinatary_id']) : $aOptions['destinatary_id'];

        $this->socket->send('speak_text', $aOptions);
        $this->closeSocket();
        return true;
    }

    public function customMessage($aOptions)
    {
        if(!$this->config['enabled'] || !$this->isAlive())
        {
            return false;
        }

        $default_options = array(
            'destinatary_id' => false,
            'callback' => ''
        );
        $aOptions = array_merge($default_options, $aOptions);
        //$aOptions['destinatary_id'] = $aOptions['destinatary_id'] ? md5($aOptions['destinatary_id']) : $aOptions['destinatary_id'];

        $this->socket->send('custom_message', $aOptions);
        $this->closeSocket();
        return true;
    }

    /*public function getNotificationIcon()
    {
        return '/bundles/xlabsspider/nodejs/notifications/spider.png';
    }

    public function getErrorNotificationIcon()
    {
        return '/bundles/xlabsspider/nodejs/notifications/error.png';
    }*/
}