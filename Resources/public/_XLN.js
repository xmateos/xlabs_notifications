var _XLN = _XLN || (function(){
    var _this;
    return {
        ownerDocument: document,
        original_title: document.title,
        _this: this,
        init: function() {
            _this = this;
        },
        users: {}, // online users
        user: {},
        socket: false,
        _set: function(param, value){
            this[param] = $.extend({}, this[param], value);
        },
        liveNotification: function(data)
        {
            data = $.extend({
                destinatary_id: false,
                title: false,
                msg: '',
                icon: false,
                muted: false,
                link: false,
                sound: false,
                fadeout_after_ms: false
                //onClick: ''
            }, data);
            //data.destinatary_id = data.destinatary_id ? md5(data.destinatary_id.toString()) : false;
            data.destinatary_id = data.destinatary_id ? data.destinatary_id : false;
            'socket' in _this && _this.socket ? _this.socket.emit('notification', data) : false;
        },
        liveSound: function(data)
        {
            data = $.extend({
                destinatary_id: false,
                file: ''
            }, data);
            data.destinatary_id = data.destinatary_id ? data.destinatary_id : false;
            'socket' in _this && _this.socket ? _this.socket.emit('play_sound', data) : false;
        },
        liveSpeak: function(data)
        {
            data = $.extend({
                destinatary_id: false,
                text: 'This is an intentional sample text'
            }, data);
            data.destinatary_id = data.destinatary_id ? data.destinatary_id : false;
            'socket' in _this && _this.socket ? _this.socket.emit('speak_text', data) : false;
        },
        customMessage: function(data)
        {
            data = $.extend({
                destinatary_id: false,
                callback: ''
            }, data);
            data.destinatary_id = data.destinatary_id ? data.destinatary_id : false;
            'socket' in _this && _this.socket ? _this.socket.emit('custom_message', data) : false;
        },
        // event test
        events: {
            onConnected: new CustomEvent('_XLN_onConnected', {
                    detail: {
                        message: 'Socket.io connected',
                        time: new Date(),
                    },
                    bubbles: true,
                    cancelable: true
                }
            ),
            onDisconnected: new CustomEvent('_XLN_onDisconnected', {
                    detail: {
                        message: 'Socket.io disconnected',
                        time: new Date(),
                    },
                    bubbles: true,
                    cancelable: true
                }
            ),
        }
    };
});