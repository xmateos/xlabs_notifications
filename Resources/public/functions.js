// HTML5 Notifications
var XLN_notifications = {};
XLN_notifications.Notification = window.Notification || window.mozNotification || window.webkitNotification;
XLN_notifications.delayTimeout = 0;
XLN_notifications.Notification.requestPermission(function(permission) {
    // console.log(permission);
});

const PERMISSION_DEFAULT = "default",
    PERMISSION_GRANTED = "granted",
    PERMISSION_DENIED = "denied",
    PERMISSION = [PERMISSION_GRANTED, PERMISSION_DEFAULT, PERMISSION_DENIED];

function checkPermission()
{
    var permission = PERMISSION_DENIED;
    if(window.webkitNotifications && window.webkitNotifications.checkPermission){
        permission = PERMISSION[window.webkitNotifications.checkPermission()];
    } else {
        if (navigator.mozNotification) {
            permission = PERMISSION_GRANTED;
        } else {
            if (window.Notification && window.Notification.permission) {
                permission = window.Notification.permission;
            }
        }
    }
    return permission;
}

function showNotification(data)
{
    if(checkPermission() === PERMISSION_GRANTED)
    {
        window.setTimeout(function(){
            var instance = new XLN_notifications.Notification(
                data.title ? data.title : "Chat Notification", {
                    body: '' + data.msg,
                    dir: 'auto',
                    //tag: 'random_id_',
                    icon: (data.icon && data.icon != '') ? data.icon : "{{ app.request.getUriForPath('') ~ asset('/bundles/xlabsnotify/xlabs.png') }}"
                }
            );

            instance.onclick = function() {
                if(data.link)
                {
                    var new_tab = window.open(data.link, '_blank');
                    new_tab.focus();
                }
                /*if(data.onClick !== undefined && window[data.onClick] === "function")
                {
                    var onClick = data.onClick;
                    if('destinatary_id' in data) delete data['destinatary_id'];
                    if('onClick' in data) delete data['onClick'];
                    window[onClick](data);
                }*/
            };
            instance.onerror = function() {
                // Something to do
            };
            instance.onshow = function() {
                // Notification sound
                if(!data.muted)
                {
                    playNotificationSound(data.sound ? data.sound : '/bundles/xlabsnotifications/sound/arpeggio.mp3');
                }
            };
            instance.onclose = function() {
                // Something to do
            };
            if(data.fadeout_after_ms)
            {
                setTimeout(instance.close.bind(instance), data.fadeout_after_ms);
            }
        }, XLN_notifications.delayTimeout);
    }
    return false;
}

function playNotificationSound(file)
{
    var player = $('audio#xlabs_notifications_player');
    if(player.length)
    {
        if(player.find('source#mp3').attr('src') != file)
        {
            player.find('source#mp3').attr('src', file);
            //player.find('source#ogg').attr('src', notification_sound[notification_sound_key].ogg);
            player.find('embed').attr('src', file);
            player[0].load();
        }
        player[0].play();
    }
}

/* Speech API - Speak stuff */
function speakText(text)
{
    //console.log(voices); // check available voices
    var synth = window.speechSynthesis;
    if(typeof synth === 'undefined') {
        return;
    }
    var voices = synth.getVoices();
    var utterThis = new SpeechSynthesisUtterance(text);
    for(i = 0; i < voices.length ; i++) {
        //if(voices[i].lang === 'es-ES') {
        if(voices[i].lang === 'en-US') {
            utterThis.voice = voices[i];
        }
    }
    utterThis.pitch = 1.2; // value between 0-2
    utterThis.rate = 1; // value between 0-2
    synth.speak(utterThis);
}

function formatBytes(a,b){if(0==a)return"0 Bytes";var c=1024,d=b||2,e=["Bytes","KB","MB","GB","TB","PB","EB","ZB","YB"],f=Math.floor(Math.log(a)/Math.log(c));return parseFloat((a/Math.pow(c,f)).toFixed(d))+" "+e[f]}