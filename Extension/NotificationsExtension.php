<?php

namespace XLabs\NotificationsBundle\Extension;

use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use XLabs\NotificationsBundle\Services\Storage;
use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

class NotificationsExtension extends AbstractExtension
{
    private $token_storage;
    private $storage;

    public function __construct(TokenStorageInterface $token_storage, Storage $storage)
    {
        $this->token_storage = $token_storage;
        $this->storage = $storage;
    }
    
    public function getFunctions()
    {
        return array(
            new TwigFunction('getTotalUserNotifications', array($this, 'getTotalUserNotifications')),
        );
    }
    
    public function getFilters()
    {
        return array();
    }

    public function getTotalUserNotifications($user_id, $not_read_only = false)
    {
        $user = $this->token_storage->getToken()->getUser();
        return is_string($user) ? 0 : $this->storage->getTotalUserNotifications($user_id, $not_read_only);
    }
}