<?php

namespace XLabs\NotificationsBundle\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class CreateNodeServerCommand extends Command
{
    protected function configure()
    {
        $this
            ->setName('xlabs_notifications:create:server')
            ->setDescription('Creates NodeJS server file')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $container = $this->getApplication()->getKernel()->getContainer();

        $bundle_config = $container->getParameter('xlabs_notifications_config');
        $rabbitmq_config = $container->getParameter('xlabs_rabbitmq_config');

        $nodejs_server_source = $container->get('templating')->render('XLabsNotificationsBundle:Server:server.js.twig', array(
            'bundle_config' => $bundle_config,
            'rabbitmq_config' => $rabbitmq_config,
            'channel_name' => md5(json_encode($bundle_config))
        ));

        // Create chat folder
        $notifications_folder = 'notifications';
        $notifications_folder = $container->get('kernel')->getRootDir().'/../web/'.$notifications_folder.'/';
        if(!is_dir($notifications_folder))
        {
            $oldmask = umask(0);
            mkdir($notifications_folder, 0775, true);
            umask($oldmask);
        }

        // Create 'node_modules' folder
        $node_modules_folder = $notifications_folder.'node_modules/';
        if(!is_dir($node_modules_folder))
        {
            $oldmask = umask(0);
            mkdir($node_modules_folder, 0775, true);
            umask($oldmask);
        }

        // Copy 'package.json' dependencies file to chat folder
        if(copy($container->get('kernel')->getRootDir().'/../web/bundles/xlabsnotifications/package.json', $notifications_folder.'package.json'))
        {
            @chmod($notifications_folder.'package.json', 0775);
        } else {
            $output->writeln('Error copying "package.json" dependencies file.');
        }

        // NodeJS server
        $json_file = $notifications_folder.'server.js';
        $file = fopen($json_file, 'w');
        fwrite($file, $nodejs_server_source);
        fclose($file);

        $output->writeln('File "'.$json_file.'" generated successfully.');

        // Gitignore file for "web/notifications" folder
        $gitignore_file = $notifications_folder.'.gitignore';
        $file = fopen($gitignore_file, 'w');
        $gitignore_file_contents = <<<EOT
*
!.gitignore
EOT;
        fwrite($file, $gitignore_file_contents);
        fclose($file);

        $output->writeln('File "'.$gitignore_file.'" generated successfully.');
    }
}