<?php

namespace XLabs\NotificationsBundle\Client;

use ElephantIO\Client as Elephant;

/**
* Client to intereact with elephant.io client
*/
class Client
{

    /** Elephant io Client */
    protected $elephantIO;

    private $xlabs_notifications_config;

    /**
     * Gets the value of elephant.
     *
     * @return \ElephantIO\Client
     */
    public function getElephantIO()
    {
        return $this->elephantIO;
    }

    /**
     * Constructor sets the value of elephant.
     * @param mixed $elephantIO the elephant io client
     *
     * @return self
     */
    public function __construct(Elephant $elephantIO, $xlabs_notifications_config)
    {
        $this->elephantIO = $elephantIO;
        $this->xlabs_notifications_config = $xlabs_notifications_config;
        return $this;
    }

    /**
     * Initialize client
     *
     * @return self
     */
    public function initialize()
    {
        $this->elephantIO->initialize();

        return $this;
    }

    /**
     * Set namespace for event
     *
     * @param string $namespace
     *
     * @return self
     */
    public function of($namespace)
    {
        $this->elephantIO->of($namespace);

        return $this;
    }

    /**
     * Emit message to socket.io
     *
     * @param string $event
     * @param array  $args
     *
     * @return $this
     */
    public function emit($event, array $args)
    {
        $this->elephantIO->emit($event, $args);

        return $this;
    }

    /**
     * Close client
     */
    public function close()
    {
        $this->elephantIO->close();
    }

    /**
     * Send to socket.io
     * @param string $eventName event name
     * @param mixed  $data      data to send must be serializable
     * @param string $namespace namespace for event
     */
    //public function send($eventName, $data, $namespace = null)
    public function send($eventName, $data, $namespace = null)
    {
        // Custom line (to make the namespace project specific)
        $namespace = '/xlabs_notifications'.md5(json_encode($this->xlabs_notifications_config));

        $this->elephantIO->initialize();

        if ($namespace !== null) {
            $this->elephantIO->of($namespace);
        }

        $this->elephantIO->emit($eventName, $data);
        $this->elephantIO->close();
    }
}
