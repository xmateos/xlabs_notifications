A redis driven like engine.

## Installation ##

Install through composer:
```bash
php -d memory_limit=-1 composer.phar require xlabs/notificationsbundle
```

In your AppKernel

```php
public function registerbundles()
{
    return [
    	...
    	...
    	new XLabs\NotificationsBundle\XLabsNotificationsBundle(),
    ];
}
```

## Routing

Append to main routing file:

``` yml
# app/config/routing.yml

x_labs_notifications:
    resource: "@XLabsNotificationsBundle/Resources/config/routing.yml"
    prefix:   /
```

## Configuration sample
Default values are shown below:
``` yml
# app/config/config.yml
  
x_labs_notifications:
    nodejs_settings:
        host: your.host.com
        port: 3026
        schema: https
        ssl_key: /etc/nginx/.../your.host.com.key
        ssl_cert: /etc/nginx/.../your.host.com.crt
        ssl_bundle: /etc/nginx/.../your.host.com.(bundle | crt)
    redis_settings:
        host: 192.168.5.23
        port: 6379
        database_id: 20
        _key_namespace: 'your:namespace:notifications'
    settings:
        message_ttl: <expiration_period_in_hours> | false
        socket_io_client_reconnect_atempts: <number_attempts> | false(default - for unlimited attempts)
```
Make sure you update all assets:
```bash
php bin/console assets:install --symlink
```

Run command to create NodeJS server file:
```bash
php bin/console xlabs_notifications:create:server
```
IMPORTANT: Make sure to run this command once you have the config set in the project. If the config changes, you will have to regenerate the server again.

Install NodeJS dependencies under "web/notifications/":
```bash
nvm install 0.10.45
nvm use 0.10.45
npm install
```

### Usage ###
Append this before the end of body tag in your template:
```php
{{ render(controller('XLabsNotificationsBundle:Notifications:loader')) }}
```

Launch the NodeJS server:
```bash
node web/notifications/server.js
```
<!--
Launch RabbitMQ consumer that stores messages in DB:
```bash
php bin/console xlabs_chat:message:store
```
Add the following command in your crontab in order to delete expired messages:
```bash
0 1 * * * php bin/console xlabs_chat:messages:expire
```
-->

### Render user notifications ###
Append this anywhere in your template:
```php
{% include 'XLabsNotificationsBundle:Frontend:notifications.html.twig' with {
    'max_results': <num_results>
} %}
```
Ajax driven pagination will be automatically added.
  
If you want to render notifications your own way, override the template by creating:
```php
app/Resources/XLabsNotificationsBundle/views/Frontend/notification.html.twig
```
To override the "no results" template:
```php
app/Resources/XLabsNotificationsBundle/views/Frontend/no_results.html.twig
```
To remove (ajax) a notification, add to your DOM element the following attribute:
```php
'data-xlabs-notifications-remove="<notification_id>"'
```
To remove (ajax) all notifications, add to your DOM element the following attribute:
```php
'data-xlabs-notifications-remove="all"'
```
To mark as read (ajax) a notification, add to your DOM element the following attribute:
```php
'data-xlabs-notifications-markAsRead="<notification_id>"'
```
You can also define a custom JS callback function  for all these operations above by adding the following attribute, so it would be executed after the action:
```php
'data-xlabs-callback="YOUR_JS_FUNCTION"'
```

### Notifications cleanup ###
It´s strongly recommended that you create a daily cronjob to remove from user notifications all the items that have automatically expired. To do so:
```php
$xlabs_storage = $container->get('xlabs_notifications_storage');
foreach($users as $user)
{
    $xlabs_storage->clearExpiredUserNotifications($user->getId())
}
```

### Requirements ###
The node js connection is made through a nginx reverse proxy. Make sure to set a nginx vhost:
```yml
server {
    listen 443 ssl;

    server_name <x_labs_chat.nodejs_settings.host>;

    ## SSL settings
    ssl on;
    ssl_certificate <x_labs_chat.nodejs_settings.ssl_cert>;
    ssl_certificate_key <x_labs_chat.nodejs_settings.ssl_key>;

    ## SSL caching/optimization
    ssl_protocols        SSLv3 TLSv1 TLSv1.1 TLSv1.2;
    ssl_ciphers RC4:HIGH:!aNULL:!MD5;
    ssl_prefer_server_ciphers on;
    keepalive_timeout    60;
    ssl_session_cache    shared:SSL:10m;
    ssl_session_timeout  10m;

	location / {
        #proxy_set_header 'Access-Control-Allow-Origin' '*';

        #proxy_set_header X-Real-IP $remote_addr;
        #proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
        #proxy_set_header Host $http_host;
        #proxy_set_header X-NginX-Proxy true;
        #proxy_redirect off;
        proxy_http_version 1.1;
        proxy_set_header Upgrade $http_upgrade;
        proxy_set_header Connection 'upgrade';
        proxy_set_header Host $host;
        proxy_cache_bypass $http_upgrade;

        proxy_pass https://<your_internal_nodejs_server_ip>:<x_labs_chat.nodejs_settings.port>;
    }
}
```