<?php

namespace XLabs\NotificationsBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Csrf\TokenStorage\TokenStorageInterface;
use Symfony\Component\Routing\Annotation\Route;
use XLabs\NotificationsBundle\Services\Notifications;
use XLabs\NotificationsBundle\Services\Storage;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;

class NotificationsController extends Controller
{
    /**
     * @Route("/loader", name="homepage")
     */
    public function loaderAction()
    {
        //$response = new Response('<!-- Notifications disabled: User not logged in -->');
        $config = $this->getParameter('xlabs_notifications_config');
        $notifications = $this->get(Notifications::class);
        $notifications_enabled = false;

        $token = $this->get('security.token_storage')->getToken();
        //if($this->container->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY'))
        if($token instanceof UsernamePasswordToken)
        {
            //$user = $this->container->get('security.token_storage')->getToken()->getUser();
            $user = $token->getUser();
            if(method_exists($user, 'getId') && $config['enabled'] && $notifications->isAlive())
            {
                $notifications_enabled = true;
            }
        }
        $response = $this->render('XLabsNotificationsBundle:Frontend:loader.html.twig', array(
            'config' => $config,
            'notifications_enabled' => $notifications_enabled,
            'channel_name' => md5(json_encode($config))
        ));
        return $response;
    }

    /* THIS WORKS BUT THE PAGINATOR MAKES IT WAY SLOWER (try to find the way to work with this solution)
    public function notificationsAction()
    {
        $notifications = array();
        if($this->container->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY'))
        {
            $user = $this->container->get('security.token_storage')->getToken()->getUser();
            $storage = $this->get('xlabs_notifier_storage');

            $post_data = stripslashes(file_get_contents("php://input"));
            $post_data = json_decode($post_data, true);

            $max_results = $post_data['max_results'] ? $post_data['max_results'] : false;
            $page = $post_data['page'] ? $post_data['page'] : 1;

            $notifications = $this->get('knp_paginator')->paginate(
                $storage->getUserNotifications($user->getId()),
                $page,
                $max_results
            );
        }
        if(count($notifications->getItems()))
        {
            $response = $this->render('XLabsNotifyBundle:Notifier:notifications_list.html.twig', array(
                'notifications' => $notifications
            ));
        } else {
            $response = $this->render('XLabsNotifyBundle:Notifier:no_results.html.twig');
        }
        return $response;
    }*/

    /**
     * @Route("/notifications", name="x_labs_notifications")
     */
    public function notificationsAction()
    {
        $count_notifications = false;
        if($this->container->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_REMEMBERED'))
        {
            $user = $this->container->get('security.token_storage')->getToken()->getUser();
            $storage = $this->get(Storage::class);

            $post_data = stripslashes(file_get_contents("php://input"));
            $post_data = json_decode($post_data, true);

            $max_results = $post_data['max_results'] ? $post_data['max_results'] : false;
            $page = $post_data['page'] ? $post_data['page'] : 1;

            $count_notifications = $storage->getTotalUserNotifications($user->getId());
            $aTemp = ($count_notifications > 0) ? array_fill(0, $count_notifications - 1, 'x') : array();

            $pagination = $this->get('knp_paginator')->paginate(
                $aTemp,
                $page,
                $max_results
            );
        }
        if($count_notifications)
        {
            $response = $this->render('XLabsNotificationsBundle:Frontend:notifications_list.html.twig', array(
                'notifications' => $storage->getUserNotifications($user->getId(), $max_results, $page),
                'notifications_pagination' => $pagination
            ));
        } else {
            $response = $this->render('XLabsNotificationsBundle:Frontend:no_results.html.twig');
        }
        return $response;
    }

    /**
     * @Route("/notification/remove", name="x_labs_notification_remove")
     */
    public function removeNotificationAction()
    {
        if($this->container->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_REMEMBERED'))
        {
            $user = $this->container->get('security.token_storage')->getToken()->getUser();
            $storage = $this->get(Storage::class);

            $post_data = stripslashes(file_get_contents("php://input"));
            $post_data = json_decode($post_data, true);

            $notification_id = $post_data['notification_id'] ? $post_data['notification_id'] : false;

            if($notification_id)
            {
                switch($notification_id)
                {
                    case 'all':
                        foreach($storage->getUserNotifications($user->getId()) as $notification)
                        {
                            //isset($notification['id']) ? $storage->remove($notification['id']) : false;
                            isset($notification['id']) ? $storage->removeUserNotification($user->getId(), $notification['id']) : false;
                        }
                        break;
                    default:
                        //$storage->remove($notification_id);
                        $storage->removeUserNotification($user->getId(), $notification_id);
                        break;
                }
            }
        }
        return new Response('ok');
    }

    /**
     * @Route("/notification/markAsRead", name="x_labs_notification_markAsRead")
     */
    public function markAsReadAction()
    {
        if($this->container->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_REMEMBERED'))
        {
            $user = $this->container->get('security.token_storage')->getToken()->getUser();
            $storage = $this->get(Storage::class);

            $post_data = stripslashes(file_get_contents("php://input"));
            $post_data = json_decode($post_data, true);

            $notification_id = $post_data['notification_id'] ? $post_data['notification_id'] : false;

            if($notification_id)
            {
                $storage->markAsRead($notification_id);
            }
        }
        return new Response('ok');
    }
}