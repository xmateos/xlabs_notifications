<?php

namespace XLabs\NotificationsBundle\DependencyInjection;

use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\HttpKernel\DependencyInjection\Extension;
use Symfony\Component\DependencyInjection\Loader;
use Symfony\Component\DependencyInjection\Definition;
use Symfony\Component\DependencyInjection\Reference;

/**
 * This is the class that loads and manages your bundle configuration.
 *
 * @link http://symfony.com/doc/current/cookbook/bundles/extension.html
 */
class XLabsNotificationsExtension extends Extension
{
    /**
     * {@inheritdoc}
     */
    public function load(array $configs, ContainerBuilder $container)
    {
        $configuration = new Configuration();
        $config = $this->processConfiguration($configuration, $configs);

        $container->setParameter('xlabs_notifications_config', $config);

        $this->loadClient('notifications', $config, $container);

        $loader = new Loader\YamlFileLoader($container, new FileLocator(__DIR__.'/../Resources/config'));
        $loader->load('services.yml');
    }

    /**
     * Load client form config
     * @param string           $name      client name
     * @param array            $config    client config
     * @param ContainerBuilder $container client name
     */
    protected function loadClient($name, array $config, ContainerBuilder $container)
    {
        $definitionIo = new Definition('ElephantIO\Client');
        $definitionIo->setPublic(false);

        $versionDefinition = new Definition('ElephantIO\Engine\SocketIO\Version1X');

        //$connection = 'ssl://'.$config['nodejs_settings']['host'].':'.$config['nodejs_settings']['port'];
        $connection = 'https://'.$config['nodejs_settings']['host'];
        $versionDefinition->addArgument($connection);
        // if issues with SSL certificate, enable line below
        //$versionDefinition->addArgument(['context' => ['ssl' => ['verify_peer_name' =>false, 'verify_peer' => false]]]);
        $versionDefinition->setPublic(false);
        $container->setDefinition('elephant_client.elephantio_version.' . $name, $versionDefinition);

        $definitionIo->addArgument(new Reference('elephant_client.elephantio_version.' . $name));

        $container->setDefinition('elephant_client.elephantio.' . $name, $definitionIo);

        $definition = new Definition('XLabs\NotificationsBundle\Client\Client');
        $definition->addArgument(new Reference('elephant_client.elephantio.' . $name));
        $definition->addArgument($config);
        //$definition->setPublic(true);

        $container->setDefinition('elephantio_client.' . $name, $definition);
        //$container->setDefinition('XLabs\NotificationsBundle\Client\Client', $definition);
    }
}
