<?php

namespace XLabs\NotificationsBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

/**
 * This is the class that validates and merges configuration from your app/config files.
 *
 * To learn more see {@link http://symfony.com/doc/current/cookbook/bundles/configuration.html}
 */
class Configuration implements ConfigurationInterface
{
    /**
     * {@inheritdoc}
     */
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder();
        $rootNode = $treeBuilder->root('x_labs_notifications');

        // Here you should define the parameters that are allowed to
        // configure your bundle. See the documentation linked above for
        // more information on that topic.
        $rootNode
            ->children()
                ->scalarNode('enabled')->defaultFalse()->end()
                ->arrayNode('nodejs_settings')->addDefaultsIfNotSet()
                    ->children()
                        ->scalarNode('host')->defaultValue('192.168.5.26')->end()
                        ->scalarNode('port')->defaultValue('3000')->end()
                        ->scalarNode('schema')->defaultValue('https')->end()
                        ->scalarNode('ssl_key')->defaultValue('/etc/nginx/ssl/607842/wildcard.bikinifanatics.com.key')->end()
                        ->scalarNode('ssl_cert')->defaultValue('/etc/nginx/ssl/607842/wildcard.bikinifanatics.com.crt')->end()
                        ->scalarNode('ssl_bundle')->defaultValue('/etc/nginx/ssl/607842/wildcard.bikinifanatics.com.bundle')->end()
                    ->end()
                ->end()
                ->arrayNode('redis_settings')->addDefaultsIfNotSet()
                    ->children()
                        ->scalarNode('host')->defaultValue('192.168.5.23')->end()
                        ->integerNode('port')->defaultValue(6379)->end()
                        ->integerNode('database_id')->defaultValue(6)->end()
                        ->scalarNode('_key_namespace')->defaultValue('xlabs:notifications')->end()
                    ->end()
                ->end()
                ->arrayNode('settings')->addDefaultsIfNotSet()
                    ->children()
                        ->scalarNode('message_ttl')->isRequired()->end()
                        ->scalarNode('socket_io_client_reconnect_atempts')->defaultFalse()->end()
                    ->end()
                ->end()
            ->end()
        ;

        return $treeBuilder;
    }
}
